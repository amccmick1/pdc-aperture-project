
-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 30, 2013 at 05:34 PM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a3573307_ach`
--

-- --------------------------------------------------------

--
-- Table structure for table `Asset`
--

DROP TABLE IF EXISTS `Asset`;
CREATE TABLE IF NOT EXISTS `Asset` (
  `AssetNo` int(6) NOT NULL AUTO_INCREMENT,
  `Active` tinyint(1) NOT NULL,
  `AssetGroupNo` int(6) NOT NULL,
  `Location` int(6) NOT NULL,
  PRIMARY KEY (`AssetNo`),
  KEY `AssetGroupNo` (`AssetGroupNo`),
  KEY `Location` (`Location`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `Asset`
--

INSERT INTO `Asset` VALUES(1, 1, 1, 1);
INSERT INTO `Asset` VALUES(2, 1, 2, 1);
INSERT INTO `Asset` VALUES(3, 1, 3, 1);
INSERT INTO `Asset` VALUES(4, 1, 4, 1);
INSERT INTO `Asset` VALUES(5, 1, 5, 1);
INSERT INTO `Asset` VALUES(6, 1, 6, 2);
INSERT INTO `Asset` VALUES(7, 1, 7, 2);
INSERT INTO `Asset` VALUES(8, 1, 8, 2);
INSERT INTO `Asset` VALUES(9, 1, 9, 2);
INSERT INTO `Asset` VALUES(10, 1, 10, 2);
INSERT INTO `Asset` VALUES(11, 1, 11, 2);
INSERT INTO `Asset` VALUES(12, 1, 12, 2);
INSERT INTO `Asset` VALUES(13, 1, 13, 3);
INSERT INTO `Asset` VALUES(14, 1, 14, 3);
INSERT INTO `Asset` VALUES(15, 1, 15, 3);
INSERT INTO `Asset` VALUES(16, 1, 16, 3);
INSERT INTO `Asset` VALUES(17, 1, 17, 3);
INSERT INTO `Asset` VALUES(18, 1, 18, 3);
INSERT INTO `Asset` VALUES(19, 1, 19, 4);
INSERT INTO `Asset` VALUES(20, 1, 20, 4);
INSERT INTO `Asset` VALUES(21, 1, 21, 4);
INSERT INTO `Asset` VALUES(22, 1, 22, 4);
INSERT INTO `Asset` VALUES(23, 1, 23, 4);
INSERT INTO `Asset` VALUES(24, 1, 24, 4);
INSERT INTO `Asset` VALUES(25, 1, 25, 5);
INSERT INTO `Asset` VALUES(26, 1, 26, 5);
INSERT INTO `Asset` VALUES(27, 1, 27, 5);
INSERT INTO `Asset` VALUES(28, 1, 28, 5);
INSERT INTO `Asset` VALUES(29, 1, 29, 5);
INSERT INTO `Asset` VALUES(30, 1, 30, 1);
INSERT INTO `Asset` VALUES(31, 1, 31, 1);
INSERT INTO `Asset` VALUES(32, 1, 32, 1);
INSERT INTO `Asset` VALUES(33, 1, 33, 1);
INSERT INTO `Asset` VALUES(34, 1, 34, 1);
INSERT INTO `Asset` VALUES(35, 1, 33, 1);
INSERT INTO `Asset` VALUES(36, 1, 32, 1);
INSERT INTO `Asset` VALUES(37, 1, 31, 1);
INSERT INTO `Asset` VALUES(38, 1, 30, 1);
INSERT INTO `Asset` VALUES(39, 1, 34, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Asset_Group`
--

DROP TABLE IF EXISTS `Asset_Group`;
CREATE TABLE IF NOT EXISTS `Asset_Group` (
  `AssetGroupNo` int(6) NOT NULL AUTO_INCREMENT,
  `Active` tinyint(1) NOT NULL,
  `Definition` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `HireCost` float(5,2) NOT NULL,
  `ImageFile` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Manufacturer` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `EngineCapacity` varchar(4) COLLATE latin1_general_ci DEFAULT NULL,
  `Category` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Transmission` tinyint(1) DEFAULT NULL,
  `Doors` tinyint(4) DEFAULT NULL,
  `Seats` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`AssetGroupNo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=35 ;

--
-- Dumping data for table `Asset_Group`
--

INSERT INTO `Asset_Group` VALUES(1, 1, 'A4', 35.00, 'audi-a4.jpg', 'Audi', '2.0', 'Estate', 0, 5, 7);
INSERT INTO `Asset_Group` VALUES(2, 1, '3 Series', 34.00, 'bmw-3series.jpg', 'BMW', '2.0', 'Estate', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(3, 1, 'Cruze', 30.00, 'chevrolet-cruze.jpg', 'Chevrolet', '2.0', 'Estate', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(4, 1, 'C5', 30.00, 'citroen-c5.jpg', 'Citroën', '2.0', 'Estate', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(5, 1, 'Focus', 31.00, 'ford-focus.jpg', 'Ford', '2.0', 'Estate', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(6, 1, 'Mondeo', 32.00, 'ford-mondeo.jpg', 'Ford', '2.0', 'Estate', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(7, 1, 'Tourer', 30.00, 'honda-tourer.jpg', 'Honda', '2.0', 'Estate', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(8, 1, 'I30 Tourer', 30.00, 'hyundai-i30-tourer.jpg', 'Hyundai', '2.0', 'Estate', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(9, 1, 'XF Sportbrake', 36.00, 'jaguar-XF-sportbrake.jpg', 'Jaguar', '2.0', 'Estate', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(10, 1, 'Focus', 28.00, 'ford-focus.jpg', 'Ford', '1.6', 'Hatchback', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(11, 1, 'A3', 28.00, 'audi-a3.jpg', 'Audi', '1.6', 'Hatchback', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(12, 1, 'Corsa', 28.00, 'vauxhall-corsa.jpg', 'Vauxhall', '1.6', 'Hatchback', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(13, 1, 'Ibiza', 29.00, 'seat-ibiza.jpg', 'Seat', '1.6', 'Hatchback', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(14, 1, 'Auris', 28.00, 'toyota-auris.jpg', 'Toyota', '1.6', 'Hatchback', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(15, 1, 'Cooper', 29.00, 'mini-cooper.jpg', 'Mini', '1.6', 'Hatchback', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(16, 1, '308', 28.00, 'peugeot-308.jpg', 'Peugeot', '1.6', 'Hatchback', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(17, 1, 'Colt', 25.00, 'mitsubishi-colt.jpg', 'Mitsubishi', '1.6', 'Hatchback', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(18, 1, 'Colt Ralliart', 27.00, 'mitsubishi-colt-ralliart.jpg', 'Mitsubishi', '1.6', 'Hatchback', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(19, 1, 'Megane', 26.00, 'renault-megane.jpg', 'Renault', '1.6', 'Hatchback', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(20, 1, 'Aygo', 25.00, 'toyota-aygo.jpg', 'Toyota', '1.2', 'Compact', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(21, 1, 'Fortwo', 22.00, 'smart-fortwo.jpg', 'Smart', '1.2', 'Compact', 1, 3, 2);
INSERT INTO `Asset_Group` VALUES(22, 1, 'Citigo', 22.00, 'skoda-citigo.jpg', 'Skoda', '1.2', 'Compact', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(23, 1, '208', 25.00, 'peugeot-208.jpg', 'Peugeot', '1.2', 'Compact', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(24, 1, 'Jazz', 25.00, 'honda-jazz.jpg', 'Honda', '1.2', 'Compact', 1, 3, 5);
INSERT INTO `Asset_Group` VALUES(25, 1, 'Passat', 35.00, 'volkswagen-passat.jpg', 'Volkswagen', '1.8', 'Saloon', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(26, 1, '6', 35.00, 'mazda-6.jpg', 'Mazda', '1.8', 'Saloon', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(27, 1, 'Avensis', 35.00, 'toyota-avensis.jpg', 'Toyota', '1.8', 'Saloon', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(28, 1, 'CC', 35.00, 'volkswagen-cc.jpg', 'Volkswagen', '1.8', 'Saloon', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(29, 1, 'Accord', 35.00, 'honda-accord.jpg', 'Honda', '1.8', 'Saloon', 1, 5, 5);
INSERT INTO `Asset_Group` VALUES(30, 1, 'GPS', 5.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `Asset_Group` VALUES(31, 1, 'Child Seat', 4.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `Asset_Group` VALUES(32, 1, 'Infant Seat', 6.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `Asset_Group` VALUES(33, 1, 'Booster Seat', 3.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `Asset_Group` VALUES(34, 1, 'Snow Tires', 10.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Booking_Detail`
--

DROP TABLE IF EXISTS `Booking_Detail`;
CREATE TABLE IF NOT EXISTS `Booking_Detail` (
  `BookingDetailID` int(6) NOT NULL AUTO_INCREMENT,
  `BookingHeadID` int(6) NOT NULL,
  `OptionalExtraCost` float(5,2) NOT NULL,
  `AssetNo` int(6) NOT NULL,
  PRIMARY KEY (`BookingDetailID`),
  KEY `BookingHeadID` (`BookingHeadID`),
  KEY `AssetNo` (`AssetNo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `Booking_Detail`
--

-- --------------------------------------------------------

--
-- Table structure for table `Booking_Header`
--

DROP TABLE IF EXISTS `Booking_Header`;
CREATE TABLE IF NOT EXISTS `Booking_Header` (
  `BookingHeadID` int(6) NOT NULL AUTO_INCREMENT,
  `Status` tinyint(4) NOT NULL,
  `UserID` int(6) NOT NULL,
  `AssetNo` int(6) NOT NULL,
  `CreationDate` datetime NOT NULL,
  `ProposedPickUpDate` datetime NOT NULL,
  `ActualPickUpDate` datetime DEFAULT NULL,
  `PickupLocation` int(6) NOT NULL,
  `StartMileage` int(6) DEFAULT NULL,
  `ProposedReturnDate` datetime NOT NULL,
  `ProposedReturnLocation` int(6) NOT NULL,
  `ActualReturnDate` datetime DEFAULT NULL,
  `ActualReturnLocation` int(6) DEFAULT NULL,
  `EndMileage` int(6) DEFAULT NULL,
  `VehicleCost` float(5,2) NOT NULL,
  `PaymentID` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`BookingHeadID`),
  KEY `UserID` (`UserID`),
  KEY `AssetNo` (`AssetNo`),
  KEY `PickupLocation` (`PickupLocation`),
  KEY `ProposedReturnLocation` (`ProposedReturnLocation`),
  KEY `ActualReturnLocation` (`ActualReturnLocation`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `Booking_Header`
--

-- --------------------------------------------------------

--
-- Table structure for table `Location`
--

DROP TABLE IF EXISTS `Location`;
CREATE TABLE IF NOT EXISTS `Location` (
  `LocationID` int(6) NOT NULL AUTO_INCREMENT,
  `Active` tinyint(1) NOT NULL,
  `Add1` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `Add2` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Add3` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Postcode` varchar(8) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`LocationID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `Location`
--

INSERT INTO `Location` VALUES(1, 1, 'Woodhouse Lane', NULL, 'Leeds', 'LS1 3HQ');
INSERT INTO `Location` VALUES(2, 1, 'Rose Bowl', 'Portland Crescent', 'Leeds', 'LS1 1JF');
INSERT INTO `Location` VALUES(3, 1, 'Merrion Centre', '5 Merrion Way', 'Leeds', 'LS2 8BT');
INSERT INTO `Location` VALUES(4, 1, 'Leeds General Infirmary', 'Clarendon Way', 'Leeds', 'LS2 9LU');
INSERT INTO `Location` VALUES(5, 1, 'Claypit Lane', 'Elmwood Road', 'Leeds', 'LS2 8AD');

-- --------------------------------------------------------

--
-- Table structure for table `Optional_Extra`
--

DROP TABLE IF EXISTS `Optional_Extra`;
CREATE TABLE IF NOT EXISTS `Optional_Extra` (
  `AssetNo` int(6) NOT NULL DEFAULT '0',
  `SerialNumber` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`AssetNo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `Optional_Extra`
--

INSERT INTO `Optional_Extra` VALUES(30, 'HXGF1001INF');
INSERT INTO `Optional_Extra` VALUES(31, 'BHASA10009');
INSERT INTO `Optional_Extra` VALUES(32, 'CBASKJB5534AS');
INSERT INTO `Optional_Extra` VALUES(33, '341ASNJ');
INSERT INTO `Optional_Extra` VALUES(34, 'ASLKNSA18866621');
INSERT INTO `Optional_Extra` VALUES(35, 'AJA88313AGGA');
INSERT INTO `Optional_Extra` VALUES(36, 'AKAS832812JSAS');
INSERT INTO `Optional_Extra` VALUES(37, 'JASA5545AFAF');
INSERT INTO `Optional_Extra` VALUES(38, 'AKSJA28641KJFA');
INSERT INTO `Optional_Extra` VALUES(39, 'AJA986SHAKS88');

-- --------------------------------------------------------

--
-- Table structure for table `Security_Question`
--

DROP TABLE IF EXISTS `Security_Question`;
CREATE TABLE IF NOT EXISTS `Security_Question` (
  `SQID` int(6) NOT NULL AUTO_INCREMENT,
  `SQ` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`SQID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `Security_Question`
--

INSERT INTO `Security_Question` VALUES(1, 'What is your mother''s maiden name?');
INSERT INTO `Security_Question` VALUES(2, 'What is the name of your favourite school teacher?');
INSERT INTO `Security_Question` VALUES(3, 'What is the name of your first pet?');
INSERT INTO `Security_Question` VALUES(4, 'Who was your childhood hero?');
INSERT INTO `Security_Question` VALUES(5, 'What was your dream job as a child?');
INSERT INTO `Security_Question` VALUES(6, 'What is the country of your ultimate dream vacation?');
INSERT INTO `Security_Question` VALUES(7, 'Where did you have your first date?');
INSERT INTO `Security_Question` VALUES(8, 'What street did you live on during primary school?');
INSERT INTO `Security_Question` VALUES(9, 'What is your oldest cousin''s first and last name?');
INSERT INTO `Security_Question` VALUES(10, 'What are the last 5 digits of your driver''s license number?');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
CREATE TABLE IF NOT EXISTS `User` (
  `UserID` int(6) NOT NULL AUTO_INCREMENT,
  `Status` tinyint(4) NOT NULL,
  `CreationDate` datetime NOT NULL,
  `AccessLvl` tinyint(4) NOT NULL,
  `Email` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `Password` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `DOB` date DEFAULT NULL,
  `Title` varchar(4) COLLATE latin1_general_ci NOT NULL,
  `FirstName` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `LastName` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Add1` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `Add2` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Add3` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Postcode` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `Telephone` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `Mobile` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `LicenseNo` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `NextLogin` datetime NOT NULL,
  `LoginAttempts` tinyint(4) NOT NULL,
  `Activation` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `SQ1` int(6) DEFAULT NULL,
  `SQ2` int(6) DEFAULT NULL,
  `SQA1` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `SQA2` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `APIToken` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `SQ1` (`SQ1`),
  KEY `SQ2` (`SQ2`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `User`
--

INSERT INTO `User` VALUES(1, 1, '2013-04-13 20:00:00', 1, 'amccmick1@googlemail.com', '$P$BA.OnIER8t3WXz2YQlgeuAXLXeFmw7.', '1988-04-28', 'Prof', 'Alex', 'McCormick', '117 East Road', NULL, 'Leeds', 'LS7 1XX', NULL, '07744050123', 'MCCO123456AB7CD', '2013-04-30 17:02:47', 0, NULL, NULL, NULL, NULL, NULL, '9207794194285714');
INSERT INTO `User` VALUES(2, 1, '2013-04-13 20:00:00', 1, 'jo.118@hotmail.com', '$P$BA.OnIER8t3WXz2YQlgeuAXLXeFmw7.', '1993-01-01', 'Mr', 'Josh', 'Chan', '35 West Phili', 'Delphia Born', 'Andraised', 'LS1 2XB', '01132412411', NULL, 'CHAN123456AB7CD', '2013-04-30 16:58:47', 0, NULL, NULL, NULL, NULL, NULL, '9204891890589200');
INSERT INTO `User` VALUES(3, 1, '2013-04-13 20:00:00', 1, 'u.ekerette8256@student.leedsmet.ac.uk', '$P$BA.OnIER8t3WXz2YQlgeuAXLXeFmw7.', '1993-01-01', 'Mr', 'Unyime', 'Ekerette', '65 North Garden', 'Parkland', 'Effel', 'LS2 1ZE', NULL, '07533551371', 'EKER123456AB7CD', '2013-04-30 06:36:18', 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `User` VALUES(4, 1, '2013-04-13 20:00:00', 1, 'stuartcrawford92@gmail.com', '$P$BA.OnIER8t3WXz2YQlgeuAXLXeFmw7.', '1993-01-01', 'Dr', 'Stuart', 'Crawford', '50 South Thisthat', 'Overwunder', 'Perhaps', 'LS11 4BE', NULL, '07755512347', 'CRAW123456AB7CD', '2013-04-30 17:30:36', 0, NULL, NULL, NULL, NULL, NULL, '9293535889246072');

-- --------------------------------------------------------

--
-- Table structure for table `Vehicle`
--

DROP TABLE IF EXISTS `Vehicle`;
CREATE TABLE IF NOT EXISTS `Vehicle` (
  `AssetNo` int(6) NOT NULL DEFAULT '0',
  `LastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `RegistrationNo` varchar(16) COLLATE latin1_general_ci NOT NULL,
  `MOTExpiry` date NOT NULL,
  `TaxExpiry` date NOT NULL,
  `Mileage` int(6) NOT NULL,
  `Available` tinyint(1) NOT NULL,
  PRIMARY KEY (`AssetNo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `Vehicle`
--

INSERT INTO `Vehicle` VALUES(1, '2013-04-25 18:24:18', 'T710EEF', '2013-10-05', '2013-09-05', 121000, 1);
INSERT INTO `Vehicle` VALUES(2, '2013-04-24 14:07:41', 'H366HEZ', '2013-09-10', '2013-12-04', 37000, 1);
INSERT INTO `Vehicle` VALUES(3, '2013-04-24 14:07:41', 'X221LOL', '2014-01-01', '2013-12-29', 40000, 1);
INSERT INTO `Vehicle` VALUES(4, '2013-04-24 14:07:41', 'X132PEI', '2013-05-06', '2014-04-29', 29100, 1);
INSERT INTO `Vehicle` VALUES(5, '2013-04-24 14:07:41', 'Z145PTY', '2013-05-06', '2014-04-11', 90000, 1);
INSERT INTO `Vehicle` VALUES(6, '2013-04-24 14:07:41', 'VA01GHI', '2014-01-07', '2014-08-11', 55000, 1);
INSERT INTO `Vehicle` VALUES(7, '2013-04-24 14:07:41', 'V613IFF', '2013-11-29', '2014-08-24', 25000, 1);
INSERT INTO `Vehicle` VALUES(8, '2013-04-24 14:07:41', 'N187IQQ', '2013-08-08', '2014-02-02', 67000, 1);
INSERT INTO `Vehicle` VALUES(9, '2013-04-24 14:07:41', 'Z921XFP', '2013-08-25', '2014-02-28', 42000, 1);
INSERT INTO `Vehicle` VALUES(10, '2013-04-24 14:07:41', 'J156NGH', '2013-06-13', '2014-02-01', 89000, 1);
INSERT INTO `Vehicle` VALUES(11, '2013-04-24 14:07:41', 'K810FFF', '2013-06-25', '2013-07-01', 32000, 1);
INSERT INTO `Vehicle` VALUES(12, '2013-04-24 14:07:41', 'P455HUG', '2013-10-13', '2013-07-30', 43000, 1);
INSERT INTO `Vehicle` VALUES(13, '2013-04-24 14:07:41', 'VA06JGI', '2013-10-17', '2013-07-27', 29000, 1);
INSERT INTO `Vehicle` VALUES(14, '2013-04-24 14:07:41', 'N117XSA', '2013-09-30', '2013-05-27', 31000, 1);
INSERT INTO `Vehicle` VALUES(15, '2013-04-24 14:07:41', 'L876ADS', '2013-09-01', '2013-06-27', 56000, 1);
INSERT INTO `Vehicle` VALUES(16, '2013-04-24 14:07:41', 'F910POF', '2013-07-13', '2013-06-23', 67000, 1);
INSERT INTO `Vehicle` VALUES(17, '2013-04-24 14:07:41', 'G399DCD', '2013-11-13', '2014-03-27', 63000, 1);
INSERT INTO `Vehicle` VALUES(18, '2013-04-24 14:07:41', 'Z131YHR', '2013-11-22', '2013-11-23', 73000, 1);
INSERT INTO `Vehicle` VALUES(19, '2013-04-24 14:07:41', 'W141SIR', '2013-08-14', '2013-12-01', 22000, 1);
INSERT INTO `Vehicle` VALUES(20, '2013-04-24 14:07:41', 'Q717BUI', '2013-11-14', '2013-08-13', 21000, 1);
INSERT INTO `Vehicle` VALUES(21, '2013-04-24 14:07:41', 'YG58FYO', '2013-09-09', '2013-06-13', 39000, 1);
INSERT INTO `Vehicle` VALUES(22, '2013-04-24 14:07:41', 'NM54378', '2013-08-08', '2013-06-09', 46000, 1);
INSERT INTO `Vehicle` VALUES(23, '2013-04-24 14:07:41', 'KN01WTH', '2013-08-07', '2013-09-13', 46500, 1);
INSERT INTO `Vehicle` VALUES(24, '2013-04-24 14:07:41', 'DN13FFS', '2013-07-11', '2014-04-12', 35400, 1);
INSERT INTO `Vehicle` VALUES(25, '2013-04-24 14:07:41', 'I448DIT', '2013-07-13', '2014-02-19', 45200, 1);
INSERT INTO `Vehicle` VALUES(26, '2013-04-24 14:07:41', 'W411TPT', '2014-05-03', '2013-12-13', 67200, 1);
INSERT INTO `Vehicle` VALUES(27, '2013-04-24 14:07:41', 'PO414ALT', '2013-05-13', '2013-11-03', 73200, 1);
INSERT INTO `Vehicle` VALUES(28, '2013-04-24 14:07:41', 'FH912FOP', '2013-07-13', '2013-09-20', 89100, 1);
INSERT INTO `Vehicle` VALUES(29, '2013-04-24 14:07:41', 'PY122THN', '2013-12-13', '2013-08-20', 91100, 1);
